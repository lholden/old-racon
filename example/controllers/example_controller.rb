class ExampleController < Controller
  mount '/example'
  #root :index

  filter :can_filter?, :only => [:filtered]

  filter do
    @hello = "Hurrah!"
  end
  
  filter :only => [:google] do
    # An example of stopping flow in a filter
    if params[:stay].nil?
      halt redirect("http://www.google.com")
      puts "*** We dont get here! ***"
    end
  end
  
  filter :only => [:archive] do
    puts "** Archive - before"
    continue
    puts "** Archive - after"
  end
  
  root
  def index
    "Hi there! #{@hello}. haz_filter? #{@haz_filter} should be nil"
  end
  
  def google
    if params[:stay].nil?
      puts "*** Halt didn't work! ***"
      "Should not get here ;)"
    else
      "stay requested, redirect bypassed"
    end
  end

  def filtered
    # Can return with a rack response
    [200, {}, "I'ma filtered yo. #{@haz_filter}"]
  end
  
  def archive(year, month, day)
    puts "** Archive - during"
    "Archive: #{year}, #{month}, #{day}"
  end
  
  def get500
    status 500
    "500: Server blew up!"
  end
  
  def haltable
    halt "I halted!"
    "I should never be rendered"
  end
  
  # perform 10 seconds worth of work, passing control back after each 'chunk'
  # Go load up another page in the browser and see that it still responds instantly.
  def long_running
    # Need streaming support at some point so we can stream a result back ;)
    100.times do
      #perform hard work!
      sleep 0.1
      continue
    end
    "finished!"
  end
protected
  def can_filter?
    @haz_filter = "Totally!"
  end
end
