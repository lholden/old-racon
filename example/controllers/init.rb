class Controller < Racon::Base
  use Rack::Session::Cookie
  use Rack::Flash, :accessorize => [:notice, :error], :sweep => true
end

Dir[File.join(File.dirname(__FILE__), '*.rb')].each do |file| 
  require file
end
