# To run: thin -R config.ru start

base_dir = File.dirname(__FILE__)
libdir = File.join(File.join(base_dir, ".."), 'lib')
$LOAD_PATH.unshift(base_dir) unless $LOAD_PATH.include?(base_dir)
$LOAD_PATH.unshift(libdir) unless $LOAD_PATH.include?(libdir)
require 'racon'
require 'http_router'
require 'controllers/init'

use Rack::ShowExceptions

router = Racon::Base.router
#router.add('/static').static(File.join(base_dir, 'public'))
run router
