# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "racon/version"

Gem::Specification.new do |s|
  s.name        = "racon"
  s.version     = Racon::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Lori Holden"]
  s.email       = ["email@loriholden.com"]
  s.homepage    = "http://rubygems.org/gems/racon"
  s.summary     = %q{A micro web-framework}
  s.description = %q{A micro web-framework relying heavily on rack and http_router for much of the heavy lifting.}

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.required_ruby_version = '1.9.2'

  s.add_runtime_dependency 'rack', '>= 1.2.1'
  s.add_runtime_dependency 'rack-fiber_pool', '>= 0.9.0'
  s.add_runtime_dependency 'rack-flash', '>= 0.1.1'
  s.add_runtime_dependency 'http_router', '~> 0.4.1'

  s.add_development_dependency 'rspec', '~> 2.0.0'
  s.add_development_dependency 'yard', '~> 0.6.0'
  s.add_development_dependency 'bundler', '~> 1.0.0'
  s.add_development_dependency 'rcov', '>= 0'
  s.add_development_dependency 'thin', '~> 1.2.7'
  s.add_development_dependency 'rerun', '~> 0.5.2'
  s.add_development_dependency 'racksh', '~> 0.9.8'
end
