class String

  # The reverse of +camelcase+. Makes an underscored of a camelcase string.
  #
  # Changes '::' to '/' to convert namespaces to paths.
  #
  # Examples
  #   "SnakeCase".snakecase           #=> "snake_case"
  #   "Snake-Case".snakecase          #=> "snake_case"
  #   "SnakeCase::Errors".snakecase   #=> "snake_case/errors"
  #
  def snakecase
    gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
  end

  # Converts a string to camelcase.
  #
  # By default camelcase upcases the first character of the string.
  # If +first_letter+ is set to +:lower+ or +true+, then +#camelcase+ will
  # produce lowerCamelCase. If it is set to +:upper+ or +nil+ it will
  # produce UpperCamelCase. If it is set to :inter or +false+ it will
  # leave the first character as given.
  #
  # +#camelcase+ also converts '/' to '::' which is useful for converting
  # paths to namespaces.
  #
  # "camel_case".camelcase #=> "CamelCase"
  # "camel/case".camelcase(:upper) #=> "Camel::Case"
  # "Camel_case".camelcase(:lower) #=> "camelCase"
  # "camel_case".camelcase(:inter) #=> "camelCase"
  # "Camel/case".camelcase(:inter) #=> "Camel::Case"
  #
  unless method_defined?(:camelcase)
    def camelcase(first_letter=nil)
      case first_letter
      when :lower, true
        lower_camelcase
      when :inter, false
        inter_camelcase
      else
        upper_camelcase
      end
    end
  end

  def inter_camelcase
    str = dup
    str.gsub!(/\/(.?)/){ "::#{$1.upcase}" }
    str.gsub!(/(?:_+|-+)([a-z])/){ $1.upcase }
    str
  end

  def upper_camelcase
    inter_camelcase.gsub(/(\A|\s)([a-z])/){ $1 + $2.upcase }
  end

  def lower_camelcase
    inter_camelcase.gsub(/(\A|\s)([A-Z])/){ $1 + $2.downcase }
  end
end
