require 'ext/string'

module Racon
  DispatchableException = Class.new(Exception)
  HaltDispatchException = Class.new(Exception)
  
  module Dispatchable
    attr_reader :params

    def self.included(base)
      unless base.ancestors.find {|a| a.name == "Racon::InheritableSettings" }
        raise Exception, "Racon::InheritableSettings is required to use #{self.name}"
      end
      
      base.extend(ClassMethods)
      
      # Provide the instance method perform_action if it's not already defined.
      unless base.instance_methods.include? :perform_action
        base.send :include, InstancePerformActionStub
      end
      
      # Provide the instance method parse_response if it's not already defined.
      unless base.instance_methods.include? :parse_response
        base.send :include, InstanceParseResponseStub
      end
    end

    def call(env)
      @env = env
      @_fiber = Fiber.current
      router_response = self.class.router.recognize(env)
      
      if router_response.nil?
        Rack::Response.new("Not Found", 404).finish
      elsif router_response.matched?
        raise DispatchableException, "racon.method not found in env." unless env['racon.method']
        @params = router_response.params_as_hash
        begin
          from_action = perform_action(env['racon.method'], *router_response.params)
        rescue HaltDispatchException => e
          from_action = e.message
        end
        parse_response(from_action)
      else
        [router_response.status, router_response.headers]
      end
    end
    
    def env
      @env
    end

    # Halt dispatch mid-flow and return a response.
    # NOTE: I wanted to use catch/throw :halt but it didn't work in filters. :/
    def halt(msg)
      raise HaltDispatchException, msg
    end
    
    def continue
      f = Fiber.current
      EM.next_tick {f.resume} if f == @_fiber
      Fiber.yield
    end
    
    module InstancePerformActionStub
      def perform_action(name, *args, &block)
        perform_action(name, *args, &block)
      end
    end
    module InstanceParseResponseStub
      def parse_response(response)
        response
      end
    end
    
    module ClassMethods
      def method_added(method)
        if public_method_defined?(method)
          case setting(:restful)
          when :singular
            singular_restful_route(method) || route_for_method(method)
          when :collection, true
            collection_restful_route(method) || route_for_method(method)
          else
            route_for_method(method)
          end << method
          if @__as_root
            last_route = precompiled_routes.last.dup
            last_route[0] = '/'
            precompiled_routes << last_route
          end
          build
        end
        reset_annotations
      end

      def mount(prefix, opts = {})
        @__prefix, @__mount_opts = prefix, opts
        self
      end

      def build
        precompiled_routes.each do |route|
          route = route.dup
          dest = route.pop
          path = route.shift
          path = File.join(@__prefix, path) if @__prefix
          router.add(path, route.last || {}).name(:"#{name.to_s.snakecase}_#{dest}").to{|env| env['racon.method'] = dest; self.call(env)}
        end
        precompiled_routes.clear
      end

      def application
        Dispatchable
      end

      def router
        router = (superclass.router if superclass.respond_to? :router) || @__router ||= HttpRouter.new
        router
      end

      def precompiled_routes
        @__precompiled_routes ||= []
      end

      def root
        @__as_root = true
      end

      def map(*args)
        @__last_map = args
      end

      protected
      def reset_annotations
        @__last_map, @__as_root = nil, nil
      end

      def singular_restful_route(m)
        opts = (@__mount_opts || {}).merge(@__last_map ? @__last_map.last : {})
        case m
        when :show;    precompiled_routes << ['/', opts.merge(:conditions => {:request_method => ['GET', 'HEAD']})]
        when :create;  precompiled_routes << ['/', opts.merge(:conditions => {:request_method => ['POST']})]
        when :update;  precompiled_routes << ['/', opts.merge(:conditions => {:request_method => ['PUT']})]
        when :destroy; precompiled_routes << ['/', opts.merge(:conditions => {:request_method => ['DELETE']})]
        else; nil
        end
        precompiled_routes.last
      end

      def collection_restful_route(m)
        opts = (@__mount_opts || {}).merge(@__last_map ? @__last_map.last : {})
        case m
        when :index;   precompiled_routes << ['/', opts.merge(:conditions => {:request_method => ['GET', 'HEAD']})]
        when :show;    precompiled_routes << ['/:id', opts.merge(:conditions => {:request_method => ['GET', 'HEAD']})]
        when :create;  precompiled_routes << ['/', opts.merge(:conditions => {:request_method => ['POST']})]
        when :update;  precompiled_routes << ['/:id', opts.merge(:conditions => {:request_method => ['PUT']})]
        when :destroy; precompiled_routes << ['/:id', opts.merge(:conditions => {:request_method => ['DELETE']})]
        else; nil
        end
        precompiled_routes.last
      end

      def route_for_method(m)
        precompiled_routes << if @__last_map && @__last_map.first.is_a?(String)
          @__last_map
        else
          path = m == :index ? '/' : m.to_s
          instance_method(m).parameters.each_with_index do |(type, name), idx|
            case type
            when :req
              path << (name == :format && idx == instance_method(m).parameters.size - 1 ? ".:format" : "/:#{name}")
            when :opt
              path << "(/:#{name})"
            when :rest
              path << "/*#{name}"
            end
          end
          @__last_map ? [path] + @__last_map : [path]
        end
        precompiled_routes.last
      end
    end
  end
end
