module Racon
  module Respondable
    StatusCodeException = Class.new(Exception)
    
  protected
    def status(code = nil)
      @_status_code = parse_status_code(code) unless code.nil?
      @_status_code ||= 200
    end
    
    def headers
      @_headers ||= {}
    end
    
    def parse_response(from_action)
      # Directly return a rack response array
      return from_action if from_action.kind_of?(Array) && from_action.length == 3
 
      [status, headers, [from_action.to_s]]
    end
    
    def redirect(url, redirect_status = :temporary_redirect)
      status redirect_status
      headers['Location'] = url
      "Redirecting to #{url}"
    end
    
  private
    def parse_status_code(symbol)
      ### 1xx Informational
      case symbol
      when 100, :continue then 100
      when 101, :switching_protocols then 101
      when 102, :processing then 102
      
      ### 2xx Success
      when 200, :ok then 200
      when 201, :created then 201
      when 202, :accepted then 202
      when 203, :non_authoritative_information then 203
      when 204, :no_content then 204
      when 205, :reset_content then 205
      when 206, :partial_content then 206
      when 207, :multi_status then 207
      when 226, :im_used then 226
      
      ### 3xx Redirection
      when 300, :multiple_choices then 300
      when 301, :moved_permanently then 301
      when 302, :found then 302
      when 303, :see_other then 303
      when 304, :not_modified then 304
      when 305, :use_proxy then 305
      when 307, :temporary_redirect then 307
        
      ### 4xx Client Error
      when 400, :bad_request then 400
      when 401, :unauthorized then 401
      when 402, :payment_required then 402
      when 403, :forbidden then 403
      when 404, :not_found then 404
      when 405, :method_not_allowed then 405
      when 406, :not_acceptable then 406
      when 407, :proxy_authentication_required then 407
      when 408, :request_timeout then 408
      when 409, :conflict then 409
      when 410, :gone then 410
      when 411, :length_required then 411
      when 412, :precondition_failed then 412
      when 413, :request_entity_too_large then 413
      when 414, :request_uri_too_long then 414
      when 415, :unsupported_media_type then 415
      when 416, :requested_range_not_satisfiable then 416
      when 417, :expectation_failed then 417
      when 422, :unprocessable_entity then 422
      when 423, :locked then 423
      when 424, :failed_dependency then 424
      when 426, :upgrade_required then 426
      
      ### 5xx Server Error
      when 500, :internal_server_error then 500
      when 501, :not_implemented then 501
      when 502, :bad_gateway then 502
      when 503, :service_unavailable then 503
      when 504, :gateway_timeout then 504
      when 505, :http_version_not_supported then 505
      when 507, :insufficient_storage then 507
      when 510, :not_extended then 510
      else
        raise StatusCodeException, "Uknown status code #{symbol}"
      end
    end
  end
end
