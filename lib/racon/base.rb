require 'rack'
require 'rack/fiber_pool'
require 'rack/flash'
require 'http_router'

require 'racon/inheritable_settings'
require 'racon/action_filters'
require 'racon/rackable'
require 'racon/dispatchable'
require 'racon/respondable'

module Racon
  class Base
    include InheritableSettings
    include Rackable
    include Dispatchable
    include ActionFilters
    include Respondable

    use Rack::FiberPool
    use Rack::MethodOverride
    use Rack::ContentType
    use Rack::ContentLength
    use Rack::CommonLogger
    
    set layout: :default
    set view_path: 'views'
    set layout_path: File.join(setting(:view_path), 'layouts')

  protected
    def flash
      env['x-rack.flash']
    end
  end
end
