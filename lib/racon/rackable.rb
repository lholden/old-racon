require 'rack'

module Racon
  module Rackable
    def self.included(base)
      base.extend(ClassMethods)
      
      # Provide the instance method 'call' if its not already defined.
      unless base.instance_methods.include? :call
        base.send :include, InstanceCallStub
      end
    end
    
    module InstanceCallStub
      def call(env)
        [200, {}, [""]]
      end
    end

    module ClassMethods
      def call(env)
        builder = Rack::Builder.new
        middleware.each { |m, args, block| builder.use(m, *args, &block) }
        builder.run(self.new)
        builder.call(env)
      end

      def use(middle, *args, &block)
        @__middleware ||= []
        @__middleware << [middle, args, block]
      end

      def middleware
        @__middleware ||= []
        if superclass.respond_to?(:middleware)
          superclass.middleware + @__middleware
        else
          @__middleware
        end
      end
    end
  end
end
