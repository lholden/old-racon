require 'fiber'
require 'ext/array'

module Racon
  module ActionFilters
    def self.included(base)
      base.extend(ClassMethods)
      
      # Provide the instance method continue if it's not already defined.
      unless base.instance_methods.include? :continue
        base.send :include, InstanceContinueStub
      end
    end
    
    module InstanceContinueStub
      protected
      def continue
        Fiber.yield
      end
    end

    module ClassMethods
      def action_filters
        @__action_filters ||= []
      end

      def filter(*args, &block)
        opts = args.extract_options!
        opts.delete(:do)

        if (!args.empty? && args.first.respond_to?(:to_sym))
          opts[:do] = args.first.to_sym
        else
          opts[:do] = block unless block.nil?
        end

        raise ArgumentError, "No filter specified" unless opts.key? :do
        raise ArgumentError, ":only and :except must not be used together" if opts.key?(:only) && opts.key?(:except)

        action_filters << opts
        opts
      end
    end
  protected
    def accumulate_all_filters
      @__all_accumulated_filters ||= (self.class.ancestors.reduce([]) do |sum, a| 
         next sum unless a.respond_to?(:action_filters)
         sum + a.action_filters.reverse
       end).reverse
    end

    def filters_for_action(name)
      @__filters_for_actions ||= {}
      @__filters_for_actions[name] ||= accumulate_all_filters.find_all do |filter|
        next if filter[:except] && filter[:except].include?(name)
        next if filter[:only] && !filter[:only].include?(name)
        true
      end
    end

    def perform_action(name, *args, &block)
      # Wrap filters into fibers
      filters = filters_for_action(name).map{|f| to_fiber(f[:do]) }
      filters.each {|f| f.resume }
      result = public_send(name, *args, &block)
      filters.each {|f| f.resume if f.alive? }
      result
    end
    
  private
    def to_fiber(proc_or_sym)
      if proc_or_sym.kind_of? Symbol
        callable = method(proc_or_sym)
        if callable.parameters.find {|a| a.first == :req }
          raise ArgumentError, "filter method must not require parameters"
        end
        Fiber.new { callable.call }
      else
        Fiber.new { instance_exec &proc_or_sym }
      end
    end
  end
end
