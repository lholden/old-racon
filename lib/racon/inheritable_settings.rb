module Racon
  module InheritableSettings
    def self.included(base)
      base.extend(ClassMethods)
    end

    def setting(key)
      self.class.setting(key)
    end

    module ClassMethods
      def set(opts)
        (@__setting ||= {}).merge! opts
      end
      def setting(key)
        @__setting ||= {}
        
        if @__setting.key?(key)
          @__setting[key]
        else
          superclass.setting(key) if superclass.respond_to?(:setting)
        end
      end
    end
  end
end
