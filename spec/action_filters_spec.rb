
require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe Racon::ActionFilters do
  let(:filterable_class) { Class.new { include Racon::ActionFilters} }
  subject {filterable_class}

  describe "#filter" do
    it "successfully creates a filter" do
      filter = subject.send(:filter) {}
      subject.send(:action_filters).should include(filter)
    end

    it "requires a method or proc" do
      lambda {subject.send(:filter)}.should raise_error(ArgumentError, "No filter specified")
    end

    it "does not allow :only and :except to be used together" do
      lambda {subject.send(:filter, :unknown_method, only: [:one], except: [:other]) }.
        should raise_error(ArgumentError, ":only and :except must not be used together")
    end

    describe "with a method defined for filtering" do
      subject do
        Class.new(filterable_class) do
        protected
          def filter_method; end
          def broken(a,b,c); end
        end
      end

      it "successfully creates a filter" do
        filter = subject.send(:filter, :filter_method)
        subject.send(:action_filters).should include(filter)
      end
    end
  end

  describe "with filters" do
    let(:filtered_instance) do
      a = Class.new(filterable_class) do
        filter(name: :all_1) {all_1_called}
        filter(name: :only_an_action_1, only: [:an_action]) {}
        filter(name: :only_other_action_other_1, only: [:other_action]) {}
        filter(name: :except_an_action_1, except: [:an_action]) {}
        def an_action; :success; end
        def other_action; end
      end
      other = Class.new(a) do
        filter(name: :all_other) {all_other_called}
        filter(name: :only_an_action_other, only: [:an_action]) {}
        filter(name: :only_other_action_other_other, only: [:other_action]) {}
        filter(name: :except_an_action_other, except: [:an_action]) {}
      end
      b = Class.new(a) do
        filter(name: :all_2) {all_2_called}
        filter(name: :only_an_action_2, only: [:an_action]) {}
        filter(name: :only_other_action_other_2, only: [:other_action]) {}
        filter(name: :except_an_action_2, except: [:an_action]) {}
      end
      c = Class.new(b) do
        filter(name: :all_3) {all_3_called}
        filter(name: :only_an_action_3, only: [:an_action]) {}
        filter(name: :only_other_action_other_3, only: [:other_action]) {}
        filter(name: :except_an_action_3, except: [:an_action]) {except_an_action_3_called}
        filter(:methodized_filter, name: :all_methodized_filter)
      protected
        def methodized_filter; end
        def invalid_filter(a, b, c); end
        def all_1_called; end
        def all_2_called; end
        def all_3_called; end
        def all_other_called; end
        def except_an_action_3_called; end
      end
      c.new
    end
    subject { filtered_instance }

    describe "#to_fiber" do
      it "raises error when method requires parameters" do
        lambda { filtered_instance.send(:to_fiber, :invalid_filter) }.
          should raise_error(ArgumentError, "filter method must not require parameters")
      end
    end

    describe "#accumulate_all_filters" do
      subject { filtered_instance.send(:accumulate_all_filters).map {|f| f[:name]} }

      it "orders filters oldest to newest" do
        subject[0].should == :all_1
        subject[1].should == :only_an_action_1
        subject.last.should == :all_methodized_filter
      end

      it "includes all inherited filters" do
        subject.should include(:all_1, :all_2, :all_3, :all_methodized_filter)
        subject.should include(:only_an_action_1, :only_an_action_2, :only_an_action_3) 
        subject.should include(:only_other_action_other_1, :only_other_action_other_2, :only_other_action_other_3)
        subject.should include(:except_an_action_1, :except_an_action_2, :except_an_action_3)
      end

      it "does not include filters for other classes" do
        subject.should_not include(:all_other, :only_an_action_other, :only_an_action_other_other, 
                                   :except_an_action_other)
      end
    end
    describe "#filters_for_action" do
      let(:filters_for_an_action) { filtered_instance.send(:filters_for_action, :an_action) }
      subject { filters_for_an_action.map {|f| f[:name]} }

      it "includes all inherited filters for action" do
        subject.should include(:all_1, :all_2, :all_3, :all_methodized_filter)
        subject.should include(:only_an_action_1, :only_an_action_2, :only_an_action_3)
      end
      
      it "does not include filters for other actions" do
        subject.should_not include(:only_other_action_other_1, :only_other_action_other_2, :only_other_action_other_3)
        subject.should_not include(:except_an_action_1, :except_an_action_2, :except_an_action_3)
      end
    end
    describe "#perform_action" do
      subject do
        o = filtered_instance.dup
        o.instance_exec do
          @methodized_filter_before = false
          @methodized_filter_after = false
          def methodized_filter
            @methodized_filter_before = true
            continue
            @methodized_filter_after = @methodized_filter_before
          end
          @all_1 = false
          def all_1_called
            @all_1 = true
          end
          @all_2 = false
          def all_2_called
            @all_2 = true
          end
          @all_3 = false
          def all_3_called
            @all_3 = true
          end
          def except_an_action_3_called
            raise "fail"
          end
          def all_other_called
            raise "fail"
          end
        end
        o
      end

      it "uses filters and calls action successfully" do
        [:@all_1,:@all_2,:@all_3].each { |v| subject.instance_variable_get(v).should be_false } 

        lambda { subject.send(:perform_action, :an_action).should == :success }.
          should_not raise_error(RuntimeError, "fail")

        [:@all_1,:@all_2,:@all_3].each { |v| subject.instance_variable_get(v).should be_true }
      end

      it "allows filters to surround the action" do
        [:@methodized_filter_before, :@methodized_filter_before].
          each { |v| subject.instance_variable_get(v).should be_false }

        subject.send(:perform_action, :an_action).should == :success

        [:@methodized_filter_before, :@methodized_filter_before].
          each { |v| subject.instance_variable_get(v).should be_true }
      end
    end
  end
end
