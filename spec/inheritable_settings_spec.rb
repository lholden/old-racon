require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

shared_examples_for "it has base settings" do
  describe "#setting" do
    it "returns existing base setting" do
      subject.setting(:spartian).should == "Drapes"
    end
    it "returns nil when setting isn't found" do
      subject.setting(:not_found).should be_nil
    end
  end
end

shared_examples_for "it can create settings" do
  describe "#set" do
    before do
      @id = subject.object_id
      subject.set :"it_can_create_settings_#{@id}" => true
      subject.set :"setting_1_#{@id}" => 1, :"setting_2_#{@id}" => 2, :"setting_3_#{@id}" => 3
    end
    it "creates one or more settings" do
      subject.setting(:"it_can_create_settings_#{@id}").should be_true
      subject.setting(:"setting_1_#{@id}").should == 1
      subject.setting(:"setting_2_#{@id}").should == 2
      subject.setting(:"setting_3_#{@id}").should == 3
    end
  end
end

describe Racon::InheritableSettings do
  describe "a class with settings" do
    let(:base_class) do
      Class.new do
        include Racon::InheritableSettings
        set spartian: "Drapes"
      end     
    end
    subject { base_class }

    it_behaves_like "it has base settings"
    it_behaves_like "it can create settings"
    
    describe "instance of base" do
      subject { base_class.new }
      it_behaves_like "it has base settings"
    end

    describe "a class inheriting a class with settings" do
      let(:inheriting_class) do
        Class.new(base_class) do
          set amazing: "grace"
        end
      end
      subject {inheriting_class}
      it_behaves_like "it has base settings"
      it_behaves_like "it can create settings"

      it "has its own settings" do
        subject.setting(:amazing).should == "grace"
      end

      it "does not disturb the class it inherits from" do
        base_class.setting(:spartian).should == "Drapes"
        base_class.setting(:amazing).should == nil
      end
    end
  end
end
