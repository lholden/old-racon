require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe Racon::Respondable do
  subject do 
    Class.new do
      include Racon::Respondable
    end.new
  end
  
  describe "#status" do
    it "defaults to 200" do
      subject.send(:status).should == 200
    end
    it "can recieve a new status"  do
      subject.send(:status).should == 200
      subject.send(:status, 400)
      subject.send(:status).should == 400
    end
    
    it "converts symbolized statuses to numeric" do
      subject.send(:status, :accepted)
      subject.send(:status).should == 202      
    end
  end
  
  describe "#parse_response" do
    it "directly returns a rack response array" do
      rack_array = [200, {}, "Hello!"]
      subject.send(:parse_response, rack_array).should == rack_array
    end
    
    it "wraps action responses in a valid rack response array" do
      subject.send(:status, :created)
      subject.send(:headers)[:test] = "moo"
      response = subject.send(:parse_response, "A body") 
      response.should == [201, {:test => "moo"}, ["A body"]]
    end
  end
end
