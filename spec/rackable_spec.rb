require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

class MockMiddleware
  def initialize(app)
    @app = app
  end
  def call(env)
    @app.call(env)
  end
  def self.apply(text)
    self.module_eval %{
      def call(env)
        res = @app.call(env)
        [res[0], res[1], ["#{text}: %s" %res[2][0]]]
      end
    }
  end
end

describe Racon::Rackable do
  let(:rackable_class) do
    Class.new do
      include Racon::Rackable
      def call(env)
        [200, {}, ["app"]]
      end
    end
  end
  subject {rackable_class}

  describe "#use" do
    subject { Class.new(rackable_class) }
    
    it "accepts new middleware" do
      middle = [lambda { [200, {}, []] }, [], nil]
      subject.middleware.should_not include(middle)
      subject.use(middle.first)
      subject.middleware.should include(middle)
    end
  end

  describe "#middleware" do
    subject do
      a = Class.new(rackable_class) { use lambda { [200, {}, ["one"]] } }
      b = Class.new(a) { use lambda { [200, {}, ["two"]] } }
      Class.new(b) { use lambda { [200, {}, ["three"]] } }
    end

    it "should return includes all inherited middleware" do
      subject.middleware.map {|m| m.first.call.last.last }.
        should include("one", "two", "three")
    end
  end

  describe "#call" do
    subject do
      a = Class.new(rackable_class) do
        use(Class.new(MockMiddleware) { apply("first") })
        use(Class.new(MockMiddleware) { apply("second") })
      end
      Class.new(a) do
        use(Class.new(MockMiddleware) { apply("third") })
      end
    end
    it "should chain inherited middleware" do
      res = Rack::MockRequest.new(subject).get('/')
      res.body.should == "first: second: third: app"
    end
  end
end
