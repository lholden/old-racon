require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe Racon::Dispatchable do
  let(:dispatchable_class) do
    Class.new do
      include Racon::InheritableSettings
      include Racon::Rackable
      include Racon::Dispatchable

      def index
        [200, {}, ['index']]
      end

      def show(id)
        [200, {}, ['show', id]]
      end
      
      def show_more(id, page = 1)
        [200, {}, ['show_more', id, page.to_s]]
      end
      
      map 'something/special-:id', :conditions => {:request_method => ['GET', 'POST']}, :id => /\d+/
      def special(id)
        [200, {}, ['special', id]]
      end
      
      map :id => /\d+/
      def just_numeric_ids(id)
        [200, {}, ['just_numeric_ids', id]]
      end
    end
  end
  subject {dispatchable_class}

  it "#call maps #index to /" do
    subject.call(Rack::MockRequest.env_for('/')).last.should == ['index']
  end

  it "#call maps #show to /show/123" do
    subject.call(Rack::MockRequest.env_for('/show/123')).last.should == ['show', '123']
  end

  it "#call maps #show to /show_more/123" do
    subject.call(Rack::MockRequest.env_for('/show_more/123')).last.should == ['show_more', '123', '1']
  end

  it "#call maps #show to /show_more/123/2" do
    subject.call(Rack::MockRequest.env_for('/show_more/123/2')).last.should == ['show_more', '123', '2']
  end

  it "#call maps #special to /something/special-2" do
    subject.call(Rack::MockRequest.env_for('/something/special-2')).last.should == ['special', '2']
  end

  it "#call maps #just_numeric_ids to /just_numeric_ids/2" do
    subject.call(Rack::MockRequest.env_for('/just_numeric_ids/2')).last.should == ['just_numeric_ids', '2']
  end

  it "#call shouldn't map #just_numeric_ids to /just_numeric_ids/asd" do
    subject.call(Rack::MockRequest.env_for('/just_numeric_ids/asd')).first.should == 404
  end

  context :rest_collection do
    let(:collection_dispatchable_class) do
      Class.new do
        include Racon::InheritableSettings
        include Racon::Rackable
        include Racon::Dispatchable

        set :restful => :collection

        def index
          [200, {}, ['index']]
        end

        def show(id)
          [200, {}, ['show', id]]
        end

        def destroy(id)
          [200, {}, ['destroy', id]]
        end

        def update(id)
          [200, {}, ['update', id]]
        end

        def create
          [200, {}, ['create']]
        end

      end
    end
    subject {collection_dispatchable_class}

    it "#call maps #index to /" do
      subject.call(Rack::MockRequest.env_for('/')).last.should == ['index']
    end

    it "#call maps #show to /123" do
      subject.call(Rack::MockRequest.env_for('/123')).last.should == ['show', '123']
    end

    it "#call maps #update to PUT /123" do
      subject.call(Rack::MockRequest.env_for('/123', :method => 'PUT')).last.should == ['update', '123']
    end

    it "#call maps #create to POST /" do
      subject.call(Rack::MockRequest.env_for('/', :method => 'POST')).last.should == ['create']
    end

    it "#call maps #destroy to DELETE /123" do
      subject.call(Rack::MockRequest.env_for('/123', :method => 'DELETE')).last.should == ['destroy', '123']
    end
  end

  context :rest_singular do
    let(:collection_dispatchable_class) do
      Class.new do
        include Racon::InheritableSettings
        include Racon::Rackable
        include Racon::Dispatchable

        set :restful => :singular

        def show
          [200, {}, ['show']]
        end

        def destroy
          [200, {}, ['destroy']]
        end

        def update
          [200, {}, ['update']]
        end

        def create
          [200, {}, ['create']]
        end

      end
    end
    subject {collection_dispatchable_class}

    it "#call maps #show to /" do
      subject.call(Rack::MockRequest.env_for('/')).last.should == ['show']
    end

    it "#call maps #update to PUT /" do
      subject.call(Rack::MockRequest.env_for('/', :method => 'PUT')).last.should == ['update']
    end

    it "#call maps #create to POST /" do
      subject.call(Rack::MockRequest.env_for('/', :method => 'POST')).last.should == ['create']
    end

    it "#call maps #destroy to DELETE /" do
      subject.call(Rack::MockRequest.env_for('/', :method => 'DELETE')).last.should == ['destroy']
    end
  end
end
